export default [
  {
    text: 'vue-bmap-gl',
    children: [
      {
        text: 'vue3版本',
        link: 'https://vue-bmap-gl.guyixi.cn'
      },
      {
        text: 'vue2版本',
        link: 'https://docs.guyixi.cn/vue-bmap-gl/#/'
      }
    ]
  },
  {
    text: 'vue-mapvgl',
    children: [
      {
        text: 'vue3版本',
        link: 'https://vue-mapvgl.guyixi.cn'
      },
      {
        text: 'vue2版本',
        link: 'https://docs.guyixi.cn/vue-mapvgl/#/'
      }
    ]
  },
  {
    text: 'vue2版本',
    link: 'https://docs.guyixi.cn/vue-amap/#/'
  },
  {
    text: '更新日志',
    link: 'https://gitee.com/guyangyang/vue-amap/blob/dev/CHANGELOG.md'
  },
  {
    text: 'gitee',
    link: 'https://gitee.com/guyangyang/vue-amap'
  },
  {
    text: 'github',
    link: 'https://github.com/yangyanggu/vue-amap/'
  }
]
